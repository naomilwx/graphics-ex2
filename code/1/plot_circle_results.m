function [] = plot_circle_results(originalCenter, originalRadius, bestCenter, bestRadius, inliers, outliers)
    function [] = plot_circle(center, radius, color)
        t = 0:0.01:2*pi; 
        xp= radius * cos(t);
        yp= radius * sin(t);
        plot(center(1) + xp, center(2) + yp, color)
    end

    function [] = plot_points(pts, color)
        for idx = 1: length(pts)
            plot(pts(idx, 1), pts(idx, 2), strcat('o', color));
        end
    end
    
    hold on;
    plot_circle(originalCenter, originalRadius, 'g');
    plot_circle(bestCenter, bestRadius, 'k');
    plot_points(inliers, 'b');
    plot_points(outliers, 'r');
end