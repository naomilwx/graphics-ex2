function [pts, center] = generate_datapoints(N, minX, maxX, minY, maxY, radius, ratio)
    rng('shuffle','twister');
    function cen = generate_random_center(radius)
        xlen = (maxX - minX) - 2 * radius;
        ylen = (maxY - minY) - 2 * radius;
        xcord = rand * xlen + minX;
        ycord = rand * ylen + minY;
        cen = [xcord ycord];
    end
    
    function inliers = generate_inliers(center, radius, num, noise)
        angles = rand(num, 1) * 2 * pi;
        inliers = zeros(num, 2);
        for i = 1: num
            angle = angles(i);
            x = center(1) + radius * cos(angle);
            y = center(2) + radius * sin(angle);
            x = x + (2 * rand - 1) * noise;
            y = y + (2 * rand - 1) * noise;
            inliers(i, 1) = x;
            inliers(i, 2) = y;
        end
    end

    function pt = generate_random_point()
        pt(1) = rand * (maxX - minX) + minX;
        pt(2) = rand * (maxY - minY) + minY;
    end

    function outliers = generate_outliers(center, radius, num, inliernoise)
        outliers = zeros(num, 2);
        
        for i = 1 : num
            pt = generate_random_point();
            while is_inlier_point(pt, center, radius, inliernoise)
                pt = generate_random_point();
            end
            outliers(i,:) = pt;
        end
    end
    
    center = generate_random_center(radius);
    pts = generate_inliers(center, radius, round((1 - ratio) * N), 0.1);
    pts = [pts; generate_outliers(center, radius, round(ratio * N), 0.1)];
end