function val = is_inlier_point(pt, center, radius, inliernoise)
    dist = norm(pt - center);
    val = dist < radius + inliernoise && dist > radius - inliernoise;
end