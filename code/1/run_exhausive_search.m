function bestInNum = run_exhausive_search(pts, originalCenter, originalRadius, ratio)
    inliernoise = 0.1;
    num = length(pts);
    combis = nchoosek(1:1:num, 3);
    bestInNum = 0;
    bestInliers = [];
    bestOutliers = [];
    bestCenter = [];
    bestRadius = 0;
    for i = 1: length(combis(:, 3))
        a = pts(combis(i, 1), :);
        b = pts(combis(i, 2), :);
        c = pts(combis(i, 3), :);
        if(check_points_are_collinear(a, b, c))
            continue;
        end
        [center, radius] = calculate_circle_center([a; b; c]);
        inliers = [];
        outliers = [];
        for idx = 1 : num
            pt = pts(idx, :);
            if is_inlier_point(pt, center, radius, inliernoise)
                inliers = [inliers; pt];
            else
                outliers = [outliers; pt];
            end
        end
        numInliers = length(inliers);
        
        if numInliers > bestInNum
            bestInNum = numInliers;
            bestInliers = inliers;
            bestOutliers = outliers;
            bestCenter = center;
            bestRadius = radius;
        end
    end
    
    
    plot_circle_results(originalCenter, originalRadius, bestCenter, bestRadius, bestInliers, bestOutliers);
    print(strcat('erad', num2str(originalRadius), 'rat', num2str(ratio)), '-dpng');
end