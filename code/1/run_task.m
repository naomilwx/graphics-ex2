function [] = run_task(type)
    N = 100;
    minX = -10;
    maxX = 10;
    minY = -10;
    maxY = 10;
    radius = 3;
    function run_ransac_task(ratio)
        [pts, center] = generate_datapoints(N, minX, maxX, minY, maxY, radius, ratio);
        [iters, res, bestInNum] = run_RANSAC_experiment(pts, center, radius, ratio);
        figure;
        bar(iters, res);
        print(strcat('graph', num2str(ratio)), '-dpng');
        close all;
    end

    function run_exhausive_task(ratio)
        [pts, center] = generate_datapoints(N, minX, maxX, minY, maxY, radius, ratio);
        bestInNum = run_exhausive_search(pts, center, radius, ratio);
        disp(strcat('ratio =', num2str(ratio)));
        disp(strcat('best number of inliers ', num2str(bestInNum))); 
        close all;
    end

    ratios = [0.05, 0.2, 0.3, 0.7];
    for i = 1: length(ratios)
        if(strcmp(type, 'ransac'))
            run_ransac_task(ratios(i));
        else
            run_exhausive_task(ratios(i));
        end
    end
end