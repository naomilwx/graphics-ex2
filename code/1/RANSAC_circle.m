function [bestInNum, bestInliers, bestOutliers, bestCenter, bestRadius] = RANSAC_circle(pts, numIter)
    rng('shuffle','twister');
    num = length(pts);
    inliernoise = 0.1;
    bestInNum = 0;
    
    function points = select_points_on_circle(pts)
        points = zeros(3, 2);
        indices = randperm(num, 3);
        for idx = 1: 3
            points(idx, :) = pts(indices(idx), :);
        end
    end
    
    function [inliers, outliers, center, radius] = single_RANSAC(pts)
        points = select_points_on_circle(pts);
        while check_points_are_collinear(points(1,:), points(2,:) , points(3,:))
            points = select_points_on_circle(pts);
        end
        [center, radius] = calculate_circle_center(points);
        inliers = [];
        outliers = [];
        for idx = 1 : num
            pt = pts(idx, :);
            if is_inlier_point(pt, center, radius, inliernoise)
                inliers = [inliers; pt];
            else
                outliers = [outliers; pt];
            end
        end
    end
        
    bestInliers = [];
    bestOutliers = [];
    bestCenter = [];
    bestRadius = 0;
    for i = 1 : numIter
       [inliers, outliers, center, radius] = single_RANSAC(pts);
       numIn = length(inliers);
       if numIn > bestInNum
           bestInNum = numIn;
           bestInliers = inliers;
           bestOutliers = outliers;
           bestCenter = center;
           bestRadius = radius;
       end
    end
end