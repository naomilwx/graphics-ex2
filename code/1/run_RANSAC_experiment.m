function [iters, res, bestInNum] = run_RANSAC_experiment(pts, originalCenter, radius, ratio)
    function numiter = calculate_num_iter(p, ratio)
        numiter = log(1 - p) / log(1 - (1 - ratio) ^ 3)
    end
    
%     maxIter = ceil(calculate_num_iter(0.99, ratio));
    maxIter = 50;
    iters = 1:1:maxIter;
    res = zeros(1, length(iters));
    bestInliers = [];
    bestOutliers = pts;
    bestCenter = [];
    bestRadius = 0;
    bestInNum = 0;
    
    for i = 1: length(iters)
        [inNum, inliers, outliers, cen, rad] = RANSAC_circle(pts, iters(i));
        res(i) = inNum;
        if inNum > bestInNum
            bestInNum = inNum;
            bestRadius = rad
            bestCenter = cen;
            bestInliers = inliers;
            bestOutliers = outliers;
        end
    end
    
    plot_circle_results(originalCenter, radius, bestCenter, bestRadius, bestInliers, bestOutliers)
    print(strcat('rad', num2str(radius), 'rat', num2str(ratio)), '-dpng');
end