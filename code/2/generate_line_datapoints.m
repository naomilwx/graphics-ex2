function [pts, grad, const] = generate_line_datapoints(N, minX, maxX, minY, maxY, ratio)
    rng('shuffle','twister');
    grad = rand * (maxY - minY)
    const = rand * (maxY - minY) + minY
    
    function inliers = generate_inliers(grad, const, num, noise)
        X = rand(1, num) * (maxX - minX) + minX;
        inliers = zeros(num, 2);
        for i = 1: num
            y = grad * X(i) + const;
            inliers(i, 1) = X(i) + (2 * rand - 1) * noise;
            inliers(i, 2) = y + (2 * rand - 1) * noise;
        end
    end

    function pt = generate_random_point()
        pt(1) = rand * (maxX - minX) + minX;
        pt(2) = rand * (maxY - minY) + minY;
    end

    function outliers = generate_outliers(grad, const, num, inliernoise)
        outliers = zeros(num, 2);
        
        for i = 1 : num
            pt = generate_random_point();
            while is_inlier_point(pt, grad, const, inliernoise)
                pt = generate_random_point();
            end
            outliers(i,:) = pt;
        end
    end

    function isInlier = is_inlier_point(pt, grad, const, inliernoise)
        expectedy = pt(1) * grad + const;
        diff = norm(pt - [pt(1) expectedy]);
        isInlier = (diff < inliernoise);
    end
    
    pts = generate_inliers(grad, const, (1 - ratio) * N, 0.1);
    pts = [pts; generate_outliers(grad, const, ratio * N, 0.1)];
end