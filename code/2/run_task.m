function [] = run_task(rtype)
    N = 100;
    minX = -10;
    maxX = 10;
    minY = -10;
    maxY = 10;
    
    ratios = [0, 0.1];
    for i = 1: length(ratios)
        ratio = ratios(i);
        [pts, origGrad, origCons] = generate_line_datapoints(N, minX, maxX, minY, maxY, ratio);
        [grad, const] = line_fitting(pts, minX, maxX, origGrad, origCons, rtype)
        print(strcat('a', num2str(origGrad), 'c', num2str(origCons), 'r', num2str(ratio)), '-dpng');
        close all;
    end
end