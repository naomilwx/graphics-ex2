function [] = plot_line_fitting_results(pts, minX, maxX, origGrad, origConst, foundGrad, foundConst)
    function [] = plot_points(pts, color)
        for idx = 1: length(pts(:, 1))
            plot(pts(idx, 1), pts(idx, 2), strcat('o', color));
        end
    end
    
    function [] = plot_line(grad, const, minX, maxX, color)
        x = minX:0.05:maxX;
        y = grad * x + const;
        plot(x, y, color);
    end

    hold on;
    plot_points(pts, 'b');
    plot_line(origGrad, origConst, minX, maxX, 'g');
    plot_line(foundGrad, foundConst, minX, maxX, 'r');
end