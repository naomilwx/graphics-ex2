function [grad, const] = line_fitting(pts, minX, maxX, origGrad, origCons, method)
    num = length(pts(:, 1));
    X = pts(:, 1);
    Y = pts(:, 2);
    A = [X ones(num, 1)];
    At = transpose(A);
    
    function [a, b] = IRLS_L1(A, At, X, Y)
        epsilon = 0.001;
        delta = 0.0001;
        weights = ones(1, num);
        
        function l1 = calculate_objective(weights, A, coeffs, Y)
            l1 = 0;
            for i = 1 : num
                l1 = l1 + abs(Y(i) - weights(i) * A(i, :) * coeffs);
            end
        end
        
        function coeffs = calculate_coeffs(weights)
            w = zeros(num, num);
            for i = 1 : num
                w(i, i) = weights(i);
            end
            coeffs =  (At * w * A) ^ -1 * (At * w * Y);
        end
        
        coeffs = calculate_coeffs(weights);
        val = calculate_objective(weights, A, coeffs, Y);
        diff = intmax;
        
        while diff > epsilon
            coeffs = calculate_coeffs(weights);
            for idx = 1: num
                weights(idx) = 1/max(delta, abs(A(idx, :) * coeffs - Y(idx)));
            end
            newVal = calculate_objective(weights, A, coeffs, Y);
            diff = abs(newVal - val);
            val = newVal;
        end
        a = coeffs(1);
        b = coeffs(2);
    end

    function [grad, const] = LP(ntype, A, X, Y)
        if strcmp(ntype, 'L1')
            f = [0; 0; ones(num,1)];
            m1 = [A, -1*eye(num)]; % -t + Ax <= y, m1t*m1*vec <= m1t*y, vec <= inv(m1tm1)m1t*y
            m2 = [-A, -eye(num)]; % -y => -t - Ax, m2t*y <= m2t*m2*vec, vec >= inv(m2tm2)m2t*y
            
            vec = linprog(f, [m1; m2], [Y; -1 * Y]);
            grad = vec(1);
            const = vec(2);
        else
            f = [0; 0; 1];
            m1 = [X, ones(num, 1), -1*ones(num, 1)];
            m2 = [-1*X, -1*ones(num, 1), -1*ones(num, 1)];
            vec = linprog(f, [m1;m2], [Y;-1*Y]);
            grad = vec(1);
            const = vec(2);
        end
    end

    if strcmp(method, 'IRLS')
        [grad, const] = IRLS_L1(A, At, X, Y);
    elseif strcmp(method, 'LPL1')
        [grad, const] = LP('L1', A, X, Y);
        disp('l1')
    else 
        [grad, const] = LP('inf', A, X, Y);
    end
    
    plot_line_fitting_results(pts, minX, maxX, origGrad, origCons, grad, const);
end